# Playbook for all of the JHB servers

## Bind.yml
Playbook for bind server.

`ansible-playbook bind.yml -b -K`

## Repos.yml
Playbook for repos server

`ansible-playbook repos.yml -b -K`
